let Categorizer = {
  labels: {},
  things_to_label: [],
  currentIndex: 0,
  started: false,
  completed: false,
  reset: function(){
    this.things_to_label = [];
    this.started = this.completed = false;
    this.currentIndex = 0;
  },
  addLabel: function () {
    this.labels[this.labelInputText] = [];
    this.labelInputText = "";
  },
  loadText: function () {
    this.reset();
    try {
      this.things_to_label = Array.from(JSON.parse(this.sourceInputText));
    } catch (error) {
      console.log(error);
      alert("Invalid JSON");
    }
  },
  eventListenerFunc: function (res, e) {
    let keycode = e.keyCode;
    if (this.keycodes.indexOf(keycode) > -1 && !this.completed) {
      let key = res[keycode];
      this.labels[key].push(this.things_to_label[this.currentIndex]);
      this.currentIndex += 1;
      console.log(this.currentIndex, this.things_to_label.length);
      if (this.currentIndex == this.things_to_label.length) {
        this.started = false;
        this.completed = true;
      }
      m.redraw();
    }
    console.log('labels', this.labels);
  }
  ,
  startLabelling: function () {
    this.completed = false;
    this.keycodes = Object.keys(this.labels).map((key, index) => {
      return (index + 1).toString().charCodeAt(0)
    });
    console.log('keycodes', this.keycodes);
    let res = _.zipObject(this.keycodes, Object.keys(this.labels));
    document.addEventListener('keydown', this.eventListenerFunc.bind(this,res), false);
    this.started = true;
  },
  labelInputText: '',
  view: function () {
    return (m("div", [
      m("h3", 'Categorizer'),
      m("div", [
        m("textarea", {
          oninput: m.withAttr('value', val => {
            this.sourceInputText = val
          }),
          value: this.sourceInputText,
          rows: 30,
          cols: 100
        })
      ]),
      m("button", {onclick: this.loadText.bind(this)}, "Load Text"),
      this.things_to_label.length > 0 ? m("p", `Loaded ${this.things_to_label.length}.`) : null,
      m("div", [
        Object.keys(this.labels).map((l, i) => {
          return m('p', `${i + 1}: ${l}`)
        }),
        m("p", this.labelInputText),
        m("input", {
          type: 'text',
          oninput: m.withAttr('value', (val)=> {
            this.labelInputText = val;
          }),
          value: this.labelInputText
        }),
        m("button", {onclick: this.addLabel.bind(this)}, "Add Label")
      ]),
      m('hr'),
      m('button', {onclick: this.startLabelling.bind(this)}, "Start Labelling"),
      this.started ? m("div", [
        m("span", this.currentIndex + 1),
        m("span", this.things_to_label[this.currentIndex]),
          m('a', {href: this.things_to_label[this.currentIndex].toString(), target: 'blank_'}, this.things_to_label[this.currentIndex])
      ]) : null,
        m('div.uk-flex.uk-flex-row', [
            Object.keys(this.labels).map(label => {
              return m('div.uk-height-large.uk-overflow-auto.uk-section.uk-section-default', [m('h3', label.toString())].concat(this.labels[label].map(item => {
                    return (m('p', item))
                  }))
              )
            })
        ]),
        this.completed ? m("div", [
            m("p", 'All Done'),
            m("p", JSON.stringify(this.labels))
        ]) : null
    ]))
  }
};
m.mount(document.body, Categorizer);