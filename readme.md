## Categorizer

Utility for manually categorizing text into different categories.


1. Load data as JSON array and click "Load Text"
2. Input your labels and click "Add Label" after each.
3. Click start labelling.


You will be shown a single entry from your array at a time. Use the keyboard to quickly assign labels. 

The first label corresponds to 1, the second to 2 and so on. Once you're done, you results are displayed as a JSON object.
